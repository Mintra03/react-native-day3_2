import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon, Flex } from '@ant-design/react-native';

class Editprofile extends Component {

    UNSAFE_componentWillMount() {
        console.log(this.props)
    }

    goToProfile = () => {
        this.props.history.push('/Profile')
    }

    goToList = () => {
        this.props.history.push('/List')
    }

    render() {
        return (
            <View style={[styles.container]}>
                <View style={[styles.header, styles.center]}>
                    <TouchableOpacity onPress={this.goToList}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="#34495E" /> </Text>
                        </View>
                    </TouchableOpacity>

                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.textHeader}> Edit Profile </Text>
                    </View>
                </View>

                <View style={[styles.content]}>
                    {/* <Text> {this.state.FirstName} {this.state.LastName} </Text> */}
                    <View style={[styles.textInput1, styles.center]}>
                        <TextInput
                            style={styles.headertext}
                            placeholder='FirstName'
                        // onChangeText={value => { this.setState({ Username: value }) }}
                        />
                    </View>

                    <View style={[styles.textInput1, styles.center]}>
                        <TextInput
                            style={styles.headertext}
                            placeholder='LastName'
                        // onChangeText={value => { this.setState({ Password: value }) }}
                        />
                    </View>
                </View>

                <View style={[styles.footer]}>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToProfile}>Save</Button>
                    </Flex.Item>
                </View>

            </View>


        )
    }
}
export default Editprofile

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'black',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: '#474644',
        flexDirection: 'row',
    },
    content: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'
    },
    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    box: {
        backgroundColor: '#F4CAF9',
        flex: 1,

        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        backgroundColor: '#F4CAF9',
        flex: 0,

    },
});
