import React, { Component } from 'react';
import { Platform, StyleSheet, Image, Text, View, Alert, TouchableHighlight, TextInput } from 'react-native';
import { Button, Icon } from '@ant-design/react-native';


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Login extends Component {

    state = {
        Username: '',
        Password: ''
    }

    onClickLogin() {
        this.props.history.push('/List', { 
        Username: this.state.Username })
    }

    render() {
        return (
            <View style={styles.container} >
                <View style={styles.content}>
                    <View style={[styles.box1, styles.center]}>
                        <View style={[styles.image, styles.logo, styles.center]}>
                            <Image source={require('./3.jpeg')} style={[styles.image, styles.logo, styles.center]} />
                        </View>
                    </View>


                    <View style={[styles.box2, styles.center]}>
                        {/* <Text> {this.state.Username} {this.state.Password} </Text> */}
                        <View style={[styles.textInput1, styles.center]}>
                            <TextInput
                                style={styles.headertext}
                                placeholder='Username'
                                onChangeText={value => {this.setState({Username:value})}}
                            />
                        </View>

                        <View style={[styles.textInput1, styles.center]}>
                            <TextInput
                                style={styles.headertext}
                                placeholder='Password' 
                                onChangeText={value => {this.setState({Password:value})}}
                                />
                        </View>

                        <View style={[styles.button, styles.center]}>
                            <Button style={[styles.button, styles.center]} title='Login'  onPress={this.onClickLogin} />
                        </View>

                    </View>
                </View>
            </View>
        );
        }
    }

export default Login


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },
    content: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'
    },
    headertext: {
        height: 40,
        borderColor: '#F5B041',
        borderWidth: 3,
        fontSize: 20,
    },
    box1: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'
    },

    box2: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 150,
        width: 250,
        height: 250
    },

    image: {
        backgroundColor: '#EC7063',
        margin: 50,
    },

    textInput1: {
        backgroundColor: '#F5B041',
        flex: 1,
        padding: 12,
        margin: 10
    },

    button: {
        backgroundColor: '#3498DB',
        flex: 1,
        margin: 40,
    },
    margin: {
        margin: 40,
    },
});
