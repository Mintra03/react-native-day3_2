import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Login from './Login'
import Profile from './Profile'
import Editprofile from './Editprofile'
import List from './List'
import Product from './Product'
import Editproduct from './Editproduct'
import Addproduct from './Addproduct'
import { store, history } from './AppStore.js'
import { Provider } from 'react-redux'
import App from './App.js'

class Router extends Component {
    render() {
        return (
            <NativeRouter> 
                {/* <Provider> */}
                {/* <ConnectedRouter history={history}> */}
                <Switch> 
                    <Route excat path='/Login' component={Login} />
                    <Route excat path='/Profile' component={Profile} />
                    <Route excat path='/Editprofile' component={Editprofile} />
                    <Route excat path='/List' component={List} />
                    <Route excat path='/Product' component={Product} />
                    <Route excat path='/Editproduct' component={Editproduct} />
                    <Route excat path='/Addproduct' component={Addproduct} />
                    <Redirect to='/Login' />
                </Switch>
                {/* </ConnectedRouter> */}
                {/* <Provider> */}
            </NativeRouter>

        )
    }
}

export default Router
